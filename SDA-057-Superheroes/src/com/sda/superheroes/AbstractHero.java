package com.sda.superheroes;

public abstract class AbstractHero {
	private String name;
	private HeroStatistics stats;
	private TeamType team;

	public AbstractHero(String name, HeroStatistics stats, TeamType team) {
		super();
		this.name = name;
		this.stats = stats;
		this.team = team;

		switch (team) {
		case RED:
			stats.addHealth(50);
			break;
		case BLUE:
			stats.addAttack(50);
			break;
		case GREEN:
			stats.addDefense(50);
			break;

		}

	}

	public String getName() {
		return name;
	}

	public HeroStatistics getStats() {
		return stats;
	}

	public TeamType getTeam() {
		return team;
	}

	public abstract double getPower();
}
