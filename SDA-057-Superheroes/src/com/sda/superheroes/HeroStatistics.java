package com.sda.superheroes;

public class HeroStatistics {
	private int health;
	private int attack;
	private int defense;

	public HeroStatistics(int health, int attack, int defense) {
		super();
		this.health = health;
		this.attack = attack;
		this.defense = defense;
	}

	public int getHealth() {
		return health;
	}

	public int getAttack() {
		return attack;
	}

	public int getDefense() {
		return defense;
	}

	public void addHealth(int toAdd) {

		this.health = this.health + toAdd;

	}

	public void addAttack(int toAdd) {
		this.attack = this.attack + toAdd;
	}

	public void addDefense(int toAdd) {
		this.defense = this.defense + toAdd;
	}
}
