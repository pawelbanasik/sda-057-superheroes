package com.sda.superheroes;

public class SuperHero extends AbstractHero {

	public SuperHero(String name, HeroStatistics stats, TeamType team) {
		super(name, stats, team);

	}

	@Override
	public double getPower() {
		return (this.getStats().getDefense() + this.getStats().getHealth()) * this.getStats().getHealth();
	}

}
