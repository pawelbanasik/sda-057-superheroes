package com.sda.teams;

import java.util.ArrayList;
import java.util.List;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.TeamType;

public class Team {

	private TeamType team;
	private List<AbstractHero> heroesList;
	private AbstractHero teamLeader;

	public Team(TeamType team) {
		super();
		this.team = team;
		heroesList = new ArrayList<AbstractHero>();
	}

	public boolean addHeroToTeam(AbstractHero hero) {
		if (hero.getTeam() == team) {
			heroesList.add(hero);
			return true;
		} else {
			System.out.println("Nie mozna dodac do druzyny.");
			return false;
		}
	}

	public AbstractHero getTeamLeader() {
		return teamLeader;
	}

	public AbstractHero findTeamLeader() {
		double maxPower = 0;

		for (int i = 0; i < heroesList.size(); i++) {
			if (heroesList.get(i).getName().equals("Batman")) {
				teamLeader = heroesList.get(i);
			}

			else if (heroesList.get(i).getPower() > maxPower) {
				maxPower = heroesList.get(i).getPower();
				teamLeader = heroesList.get(i);
			}

		}
		return teamLeader;
	}

	@Override
	public String toString() {
		boolean leaderFound = false;
		StringBuilder sb = new StringBuilder();

		for (AbstractHero hero : heroesList) {
			sb.append(hero.getName());
			if (hero == teamLeader) {
				sb.append(" >Leader< ");
				
			}
			sb.append("\n");

		}
		return sb.toString();
	}

}
