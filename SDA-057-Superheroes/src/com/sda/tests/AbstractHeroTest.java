package com.sda.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.TeamType;
import com.sda.superheroes.Villain;

public class AbstractHeroTest {

	@Test
	public void testHeroCreation() {
		Villain villain = new Villain("Rysiek", new HeroStatistics(100, 90, 80), TeamType.RED);
		SuperHero superHero = new SuperHero("Franek", new HeroStatistics(100, 75, 85), TeamType.BLUE);

		assertTrue((villain.getStats().getHealth()) == 150);
	}
}
