package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.TeamType;
import com.sda.teams.Team;
import com.sda.utils.HeroCreator;

public class TeamTest {

	@Test
	public void addHeroToTeamTest() {

		Team team = new Team(TeamType.BLUE);

		// assertTrue(team.addHeroToTeam(HeroCreator.createHeroWithDefaultStats("Pawel",
		// TeamType.BLUE)));
		assertTrue(team
				.addHeroToTeam(HeroCreator.createSuperHero("Rysiu", new HeroStatistics(30, 50, 90), TeamType.BLUE)));
		assertFalse(
				team.addHeroToTeam(HeroCreator.createSuperHero("Rysiu", new HeroStatistics(30, 50, 90), TeamType.RED)));
	}

	public void findTeamLeaderTest() {

		Team team = new Team(TeamType.BLUE);
		team.addHeroToTeam(HeroCreator.createSuperHero("Rysiu", new HeroStatistics(40, 50, 90), TeamType.BLUE));
		team.addHeroToTeam(HeroCreator.createSuperHero("Batman", new HeroStatistics(30, 50, 90), TeamType.BLUE));

		assertTrue(team.findTeamLeader().getName().equals("Batman"));
		assertFalse(team.findTeamLeader().getName().equals("Rysiek"));

	}
}
