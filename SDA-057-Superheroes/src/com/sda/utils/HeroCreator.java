package com.sda.utils;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.TeamType;
import com.sda.superheroes.Villain;

public class HeroCreator {

	public static SuperHero createSuperHero(String name, HeroStatistics stats, TeamType team){
		
		SuperHero superHero = new SuperHero(name, stats, team);
		return superHero;
	}
	
	public static Villain createVillain(String name, HeroStatistics stats, TeamType team){
		
		Villain villain = new Villain(name, stats, team);
		return villain;
	}
	
	public static SuperHero createHeroWithDefaultStats(String name, TeamType team){

		PropertyReader.loadPropertyValues();
		Integer defaultHealth = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
		Integer defaultAttack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
		Integer defaultDefense = Integer.parseInt(System.getProperty("config.superHeroBaseDefense"));
	
		return new SuperHero(name, new HeroStatistics(defaultHealth, defaultAttack, defaultDefense), team);
		
	}
	
	public static Villain createVillainWithDefaultStats(String name, TeamType team){
		PropertyReader.loadPropertyValues();
		Integer defaultHealth = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
		Integer defaultAttack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
		Integer defaultDefense = Integer.parseInt(System.getProperty("config.superHeroBaseDefense"));
	
		return new Villain(name, new HeroStatistics(defaultHealth, defaultAttack, defaultDefense), team);
		
	}
	
}
